FROM openjdk:8-jdk-alpine
EXPOSE 8080
COPY build/libs/*.jar shop.jar
ENTRYPOINT ["java", "-jar", "shop.jar"]
