# iMedia24 Coding challenge

### How to use it

* Start by cloning the project from https://gitlab.com/sm.bouaddi/imedia24-shop-challege.git.
* Run `gradlew bootJar` to build and create a jar file for the project.

To run on a docker container

Create a docker image using dockerfile by running the command:
```
    docker build -t app/shop
```
Start the image in a docker container using the tag "app/shop"
```
    docker run -p 8080:8080 app/shop
```


### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Official Kotlin documentation](https://kotlinlang.org/docs/home.html)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/#build-image)
* [Flyway database migration tool](https://flywaydb.org/documentation/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

