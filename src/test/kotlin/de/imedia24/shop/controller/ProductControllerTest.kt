package de.imedia24.shop.controller

import com.fasterxml.jackson.databind.ObjectMapper
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequestUpdate
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup
import java.math.BigDecimal
import java.time.ZonedDateTime


@SpringBootTest
@AutoConfigureMockMvc
internal class ProductControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @Test
    fun `should return stauts NOT FOUND if there is no product with the given sku`() {
        val sku = "24"

        mockMvc.get("/products/$sku")
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
            }
    }

    @Test
    fun `should return the product with the given sku`() {
        val sku = "24"

        mockMvc.get("/products/$sku")
            .andDo { print() }
            .andExpect {
                status { isOk() }
            }
    }

    @Test
    fun `should return a list of products with the given list of Skus`() {
        val sku = listOf("123", "4567", "8901", "2345", "67789")

        mockMvc.get("/products?skus=$sku")
            .andDo { print() }
            .andExpect {
                status { isOk() }
            }
    }

    @Test
    fun `should add a product and return it`() {
        val newProduct = ProductEntity(
            "25", "P-25", "Product 25", BigDecimal(399), 30,
            ZonedDateTime.now(), ZonedDateTime.now())

        mockMvc.post("/products"){
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(newProduct)
        }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content { contentType(MediaType.APPLICATION_JSON_UTF8) }
            }
    }

    @Test
    fun`should not allow POST if sku already exists`() {
        val newProduct = ProductEntity(
            "25", "P-25", "Product 25", BigDecimal(399), 30,
            ZonedDateTime.now(), ZonedDateTime.now())

        mockMvc.post("/products"){
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(newProduct)
        }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content { contentType(MediaType.APPLICATION_JSON_UTF8) }
            }
    }

    @Test
    fun`should update product with the given sku`() {
        val newProduct = ProductRequestUpdate(
            "25", "P-25", BigDecimal(399))
        val sku = 24

        mockMvc.put("/products/${sku}"){
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(newProduct)
        }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content { contentType(MediaType.APPLICATION_JSON_UTF8) }
            }
    }

}