package de.imedia24.shop.service

import de.imedia24.shop.Exceptions.ProductExistsException
import de.imedia24.shop.Exceptions.ProductNotFoundException
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductRequestUpdate
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        val product: ProductEntity? = productRepository.findBySku(sku)
        return product?.toProductResponse()
    }

    fun findProductBySkus(skus: List<String>): List<ProductResponse?>{
        if(productRepository.findBySkuIn(skus) == null
            || productRepository.findBySkuIn(skus).size == 0)
                throw ProductNotFoundException("No product matchs the given Skus [${skus.joinToString { it }}]")
        return productRepository.findBySkuIn(skus).map { it?.toProductResponse() }
    }

    fun addProduct(product: ProductRequest): ProductResponse {
        if(productRepository.findById(product.sku).isPresent())
            throw ProductExistsException("A product with sku ${product.sku} exists already")
        val p: ProductEntity = productRepository.save(product.toProductEntity())
        return p.toProductResponse()
    }

    fun updateProduct(sku: String, product: ProductRequestUpdate){
        if(!productRepository.findById(sku).isPresent)
            throw ProductNotFoundException("The product with sku '${sku}' does not exist")

        productRepository.updateProduct(sku, product)
    }
}
