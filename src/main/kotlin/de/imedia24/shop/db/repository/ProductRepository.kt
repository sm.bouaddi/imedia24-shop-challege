package de.imedia24.shop.db.repository

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequestUpdate
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface ProductRepository : CrudRepository<ProductEntity, String> {

    fun findBySku(sku: String): ProductEntity?

    fun findBySkuIn(skus: List<String>): List<ProductEntity?>

    @Transactional
    @Modifying
    @Query("Update ProductEntity p set p.name = :#{#product.name}, p.description = :#{#product.description}," +
            " p.price = :#{#product.price} where sku = :sku")
    fun updateProduct(
        @Param("sku") sku: String,
        @Param("product") product: ProductRequestUpdate)
}