package de.imedia24.shop.domain.product

import java.math.BigDecimal
import java.math.BigInteger

class ProductRequestUpdate(
    val name: String,
    val description: String?,
    val price: BigDecimal
)
