package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequestUpdate
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import javax.websocket.server.PathParam

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(@RequestParam skus: List<String>) : ResponseEntity<List<ProductResponse?>>{
        logger.info("Request for list of product $skus")

        val products = productService.findProductBySkus(skus)

        return if(products.size == 0) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun addProduct(@RequestBody product: ProductRequest) : ProductResponse{
        logger.info("Request for adding a new product ${product.toString()}")

        val p =  productService.addProduct(product)

        return p
    }

    @PutMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    @ResponseStatus(HttpStatus.OK)
    fun updateProduct(
        @PathVariable("sku") sku: String,
        @RequestBody product: ProductRequestUpdate
    ){
        logger.info("Request for updating the product with sku '${sku}'")

        productService.updateProduct(sku, product)

        logger.info("the product with sku '${sku}' was updated successfully")
    }
}
