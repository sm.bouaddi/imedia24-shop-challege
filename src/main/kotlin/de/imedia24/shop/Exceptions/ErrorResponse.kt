package de.imedia24.shop.Exceptions

import org.springframework.http.HttpStatus
import java.time.ZonedDateTime

class ErrorResponse (var message: String?, var timeStamp: ZonedDateTime)