package de.imedia24.shop.Exceptions

import java.lang.RuntimeException

class ProductNotFoundException(message: String) : RuntimeException(message) {

}
