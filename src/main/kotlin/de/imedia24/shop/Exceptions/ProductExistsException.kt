package de.imedia24.shop.Exceptions

import java.lang.RuntimeException

class ProductExistsException(message: String) : RuntimeException(message) {

}
