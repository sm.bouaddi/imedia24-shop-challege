package de.imedia24.shop.Exceptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.time.ZonedDateTime

@ControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(ProductExistsException::class)
    fun handleProductExistsException(e: ProductExistsException): ResponseEntity<ErrorResponse> {
        return buildErrorResponse(HttpStatus.BAD_REQUEST, e.message)
    }

    @ExceptionHandler(ProductNotFoundException::class)
    fun handleProductNotFoundException(e: ProductNotFoundException): ResponseEntity<ErrorResponse> {
        return buildErrorResponse(HttpStatus.NOT_FOUND, e.message)
    }

    fun buildErrorResponse(status: HttpStatus, message: String?): ResponseEntity<ErrorResponse> {
        val error = ErrorResponse(
            message,
            ZonedDateTime.now()
        )
        return ResponseEntity(error, status)
    }
}